package app.ecommerce.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.MainSightAdapter;
import app.ecommerce.ui.adapter.ParisAdapter;
import app.ecommerce.ui.model.MainSightModel;
import app.ecommerce.ui.model.ParisModel;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity {

    private ImageButton rowDown;
    private ImageButton rowUp;

    private LinearLayout trackMainLinearLayout;

    // Recycler View object
    private RecyclerView horizontalParisRecyclerView;
    private RecyclerView horizontalMainSightRecyclerView;

    // Array list for recycler view data source
    private List<ParisModel> parisModelList;
    private List<MainSightModel> mainSightModelList;

    // Layout Manager
    private  RecyclerView.LayoutManager RecyclerViewLayoutManager;
    private  RecyclerView.LayoutManager RecyclerViewLayoutManager2;

    // adapter class object
    private ParisAdapter parisAdapter;
    private MainSightAdapter mainSightAdapter;

    // Linear Layout Manager
    private LinearLayoutManager HorizontalLayout;
    private LinearLayoutManager HorizontalLayout2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        // initialisation with id's
        horizontalParisRecyclerView = findViewById( R.id.horizontal_paris_layout_recycler);
        horizontalMainSightRecyclerView = findViewById( R.id.horizontal_main_sight_layout_recycler);
        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        RecyclerViewLayoutManager2 = new LinearLayoutManager(getApplicationContext());

        // Set LayoutManager on Recycler View
        horizontalParisRecyclerView.setLayoutManager(RecyclerViewLayoutManager);
        horizontalMainSightRecyclerView.setLayoutManager(RecyclerViewLayoutManager2);

        // Adding items to RecyclerView.
        AddItemsToRecyclerViewArrayList();

        // calling constructor of adapter
        // with source list as a parameter
        parisAdapter = new ParisAdapter(parisModelList);
        mainSightAdapter = new MainSightAdapter(mainSightModelList);

        // Set Horizontal Layout Manager
        // for Recycler view
        HorizontalLayout = new LinearLayoutManager(ActivityDayPlans.this
                ,LinearLayoutManager.HORIZONTAL,false);
        HorizontalLayout2 = new LinearLayoutManager(ActivityDayPlans.this
                ,LinearLayoutManager.HORIZONTAL,false);
        horizontalParisRecyclerView.setLayoutManager(HorizontalLayout);
        horizontalMainSightRecyclerView.setLayoutManager(HorizontalLayout2);


        // Set adapter on recycler view
        horizontalParisRecyclerView.setAdapter(parisAdapter);
        horizontalMainSightRecyclerView.setAdapter(mainSightAdapter);

        initToolbar();
        iniComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        //change menu button color
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.grey_60));
        //change overflow menu button color
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.grey_60));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    private void iniComponent() {
        rowDown = findViewById(R.id.main_down_info);
        rowUp = findViewById(R.id.main_up_info);
        trackMainLinearLayout = findViewById(R.id.track_main);


        final Animation slideUp = AnimationUtils.loadAnimation(ActivityDayPlans.this, R.anim.slide_up);
        final Animation slideDown = AnimationUtils.loadAnimation(ActivityDayPlans.this, R.anim.slide_down);
        // on row down clicked
        rowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackMainLinearLayout.setVisibility(View.VISIBLE);
                rowUp.setAnimation(slideUp);
                rowDown.startAnimation(slideDown);
                trackMainLinearLayout.startAnimation(slideDown);
                rowUp.setVisibility(View.VISIBLE);
                rowDown.setVisibility(View.GONE);
            }
        });

        // on row Up clicked
        rowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackMainLinearLayout.startAnimation(slideUp);
                rowUp.startAnimation(slideUp);
                rowDown.startAnimation(slideDown);
                trackMainLinearLayout.setVisibility(View.GONE);
                rowUp.setVisibility(View.GONE);
                rowDown.setVisibility(View.VISIBLE);
            }
        });
    }

    private void AddItemsToRecyclerViewArrayList(){
        parisModelList = new ArrayList<>();
        parisModelList.add(new ParisModel(R.drawable.image_1));
        parisModelList.add(new ParisModel(R.drawable.image_2));
        parisModelList.add(new ParisModel(R.drawable.image_3));
        parisModelList.add(new ParisModel(R.drawable.image_4));
        parisModelList.add(new ParisModel(R.drawable.image_5));
        parisModelList.add(new ParisModel(R.drawable.image_6));
        parisModelList.add(new ParisModel(R.drawable.image_7));


        mainSightModelList = new ArrayList<>();
        mainSightModelList.add(new MainSightModel(R.drawable.image_5));
        mainSightModelList.add(new MainSightModel(R.drawable.image_6));
        mainSightModelList.add(new MainSightModel(R.drawable.image_7));
        mainSightModelList.add(new MainSightModel(R.drawable.image_1));
        mainSightModelList.add(new MainSightModel(R.drawable.image_3));
        mainSightModelList.add(new MainSightModel(R.drawable.image_4));
        mainSightModelList.add(new MainSightModel(R.drawable.image_2));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.example_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
