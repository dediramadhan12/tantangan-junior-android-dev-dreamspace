package app.ecommerce.ui.model;

public class MainSightModel {
    private int itemMainSightImage;

    public MainSightModel(int itemMainSightImage) {
        this.itemMainSightImage = itemMainSightImage;
    }

    public int getItemMainSightImage() {
        return itemMainSightImage;
    }

    public void setItemMainSightImage(int itemMainSightImage) {
        this.itemMainSightImage = itemMainSightImage;
    }
}
