package app.ecommerce.ui.model;

public class ParisModel {
    private int itemParisImage;

    public ParisModel( int itemParisImage) {
        this.itemParisImage = itemParisImage;
    }

    public int getItemParisImage() {
        return itemParisImage;
    }

    public void setItemParisImage(int itemParisImage) {
        this.itemParisImage = itemParisImage;
    }
}
