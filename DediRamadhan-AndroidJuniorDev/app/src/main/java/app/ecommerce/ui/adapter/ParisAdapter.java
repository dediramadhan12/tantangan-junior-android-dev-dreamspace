package app.ecommerce.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.ParisModel;


public class ParisAdapter extends RecyclerView.Adapter<ParisAdapter.ViewHolder> {
    private List<ParisModel> parisModelList;

    public ParisAdapter(List<ParisModel> parisModelList) {
        this.parisModelList = parisModelList;
    }

    @NonNull
    @Override
    public ParisAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_scroll_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int parisResource = parisModelList.get(position).getItemParisImage();

        holder.setItemImage(parisResource);
    }

    @Override
    public int getItemCount() {
        return parisModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView itemParisImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemParisImage = itemView.findViewById(R.id.horizontal_item_img);
        }

        private void setItemImage(int parisResource){
            itemParisImage.setImageResource(parisResource);
        }
    }


}
