package app.ecommerce.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.MainSightModel;

public class MainSightAdapter extends RecyclerView.Adapter<MainSightAdapter.ViewHolder>{
    private List<MainSightModel> mainSightModelList;

    public MainSightAdapter(List<MainSightModel> mainSightModelList) {
        this.mainSightModelList = mainSightModelList;
    }

    @NonNull
    @Override
    public MainSightAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_scroll_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int mainSightResource = mainSightModelList.get(position).getItemMainSightImage();

        holder.setItemImage(mainSightResource);
    }

    @Override
    public int getItemCount() {
        return mainSightModelList.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder{
       private ImageView itemMainSightImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemMainSightImage = itemView.findViewById(R.id.horizontal_item_img);
        }

        private void setItemImage(int mainSightResource){
            itemMainSightImage.setImageResource(mainSightResource);
        }
    }
}
